#!/bin/bash

set -euo pipefail

RPMS="$(rpm --eval '%{_rpmdir}')"
DEST="/build/RPMS"

cp -fr "${RPMS}" "${DEST}"

