## Simple and dirty helper script to build rpm packages in container

### Requirements:
- `podman`

### Basic usage:
1. Inside of directory containing spec file and patches run: `start_env.sh [image:tag]`
2. After creation of environment run: `fast_build` (NOTE: script might show linter errors after successful packages build)
3. If built rpms are required outside of container run `fetch_rpms`. Created packages will be saved in `RPMS` directory where container was started
