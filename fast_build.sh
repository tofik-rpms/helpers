#!/bin/bash

set -euo pipefail


cleanup(){
	rm -fr "${TMPDIR}"
}

run_linter() {
	local pkglist="${1}"
	local pkg
	while read pkg; do
		pkg="/${pkg#*/}"
		if [[ "${pkg}" =~ src\.rpm$ ]]; then
			continue
		fi
		printf "%s\n" "${pkg}"
		rpmlint "${pkg}"
	done < <(grep Wrote: ${pkglist})
}


TMPDIR="$(mktemp -d)"

trap cleanup EXIT

for s in *.spec; do
	tmpfile="${TMPDIR}/${s}"
	dnf -y --refresh builddep ${s}
	populate_sources "${s}"
	rpmbuild -ba ${s} | tee "${tmpfile}"
	run_linter "${tmpfile}"
	rm -f "${tmpfile}"
done
