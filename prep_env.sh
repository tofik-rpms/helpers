#!/bin/bash

source /etc/os-release

if [[ "${ID}" = "fedora" ]]; then
	dnf install -y \
		rpmdevtools dnf-plugins-core fedora-review \
		rpkg
elif [[ "${ID}" = "rocky" ]]; then
	dnf install -y 'dnf-command(config-manager)'
	dnf config-manager --enable devel
	dnf install -y \
		rpmdevtools dnf-plugins-core rpmlint \
		rpkg
else
	printf "Not supported distribution.\n"
	exit 1
fi

if [[ -f "/env.sh" ]]; then
	source /env.sh
fi

