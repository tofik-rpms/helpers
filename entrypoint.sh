#!/bin/bash

/helpers/prep_env.sh

ln -s /helpers/populate_sources.sh /bin/populate_sources
ln -s /helpers/fast_build.sh /bin/fast_build
ln -s /helpers/fetch_rpms.sh /bin/fetch_rpms


exec -l "/bin/bash"
