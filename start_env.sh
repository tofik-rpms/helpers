#!/bin/bash

HELPERS_DIR=$(dirname $(readlink -nf ${0}))
IMAGE=${1:-fedora:latest}
REPO_ROOT="$(git rev-parse --show-toplevel)"
EXTRA_OPTS=()

if [[ -f "${REPO_ROOT}/env.sh" ]]; then
	EXTRA_OPTS+=( --volume "${REPO_ROOT}/env.sh:/env.sh:ro" )
fi

podman run \
	--interactive \
	--tty \
	--volume ./:/build \
	--volume "${HELPERS_DIR}:/helpers" \
	--workdir /build \
	--entrypoint /helpers/entrypoint.sh \
	"${EXTRA_OPTS[@]}" \
	--rm ${IMAGE}

