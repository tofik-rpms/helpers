#!/bin/bash

set -x


function get_repository {
	local spec="${1}"
	local vcs url
	vcs="$(rpmspec -q --queryformat '%{vcs}' --srpm "${spec}")"
	url="$(rpmspec -q --queryformat '%{url}' --srpm "${spec}")"
    if [[ "${vcs}" = '(none)' ]]; then
		printf "%s" "${url}"
	else
		printf "%s" "${vcs}"
	fi
	return 0
}

function current_version {
	local spec="${1}"
	rpmspec -q --queryformat '%{version}' --srpm "${spec}"
}

function git_version {
	local repo="${1}"
	git ls-remote --sort='version:refname' --tags --refs "${repo}" \
		| awk -F'/' '{gsub(/^v/,"",$NF); print $NF}' \
		| tail -n1
	return 0
}

function update_spec_file {
	local line
	local gitver="${2}"
	while read line; do
		if [[ "${line}" =~ ^Version: ]]; then
			line="$(echo "${line}" | perl -CS -pe "s/(\d+\.)*\d+$/${gitver}/")"
		fi
		if [[ "${line}" =~ ^Release: ]]; then
			line="$(echo "${line}" | perl -CS -pe 's/\d+/1/')"
		fi
		printf "%s\n" "${line}"
		if [[ "${line}" = '%changelog' ]]; then
			cat <<-EOF
			* $(LANG=C date +'%a %b %d %Y') ${MAINTAINER} - ${gitver}-1
			- Update to ${gitver}

			EOF
		fi
	done < "${1}"
}

declare -a SPEC_FILES
mapfile -t SPEC_FILES < <(find $(pwd) -name '*.spec' -type f)

for spec in "${SPEC_FILES[@]}"; do
	repourl="$(get_repository "${spec}")"
	gitver="$(git_version "${repourl}")"
	rpmdev-vercmp "$(current_version "${spec}")" "${gitver}" > /dev/null
	status=$?
	[[ ${status} -ne 12 ]] && continue
	cp -f "${spec}" "${spec}.old"
	update_spec_file "${spec}.old" "${gitver}" > "${spec}"
	rm -f "${spec}.old"
done
