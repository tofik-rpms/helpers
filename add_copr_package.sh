#!/bin/bash

set -euo pipefail



if [[ $# -ne 2 ]]; then
	cat <<-EOF
	Usage: ${0} copr_name package_name
	EOF
	exit 1
fi

COPR_NAME="${1}"
PACKAGE_NAME="${2}"


GIT_REMOTE="$(git config --get remote.origin.url)"

GIT_URL="${GIT_REMOTE#*@}"
GIT_URL="${GIT_URL/:/\/}"

if [[ ! "${GIT_URL}" =~ ^https?:// ]]; then
	GIT_URL="https://${GIT_URL}"
fi

printf "%s\n" "${GIT_URL}"



copr add-package-scm \
	--name "${PACKAGE_NAME}" \
	--spec "${PACKAGE_NAME}.spec" \
	--subdir "${PACKAGE_NAME}" \
	--clone-url "${GIT_URL}" \
	"${COPR_NAME}"
