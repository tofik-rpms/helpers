#!/bin/bash

SOURCEDIR="$(rpm --eval '%{_sourcedir}')"

[[ -d "${SOURCEDIR}" ]] || mkdir -p "${SOURCEDIR}"

SPECFILES=( $(ls *.spec) )

if [[ $# -ne 0 ]]; then
	SPECFILES=( "$@" )
fi

for s in "${SPECFILES[@]}"; do
	while read; do
		p=${REPLY#*: }
		echo "${p}"
		if [[ -f "${SOURCEDIR}/${p##*/}" ]]; then
			continue
		fi
		if [[ "${p}" =~ ^(https?|ftps?|sftp):// ]]; then
			f="${p##*/}"
			curl --output "${SOURCEDIR}/${f}" -L "${p}"
		else
			ln -fs "$(pwd)/${p}" "${SOURCEDIR}/${p}"
		fi
	done < <(spectool -a "${s}")
done
